def get_remote_base(local_dir='', permanent=True, machine='a'):
    from os.path import join
    if not permanent:
        return join(f"/data/scratch/jh210022{machine}/share/bigdft",local_dir)
    else:
        return join(f"/work/jh210022{machine}/share/bigdft/hiv-runs-shell", local_dir)

def get_computers(hostname, **extra_kwargs):
    from yaml import load, Loader
    from remotemanager import BaseComputer
    from gentool.template import wisteria_spec
    from os.path import abspath, realpath, join, dirname
    basedir = abspath(dirname(realpath(__file__)))
    with open(join(basedir, hostname+'.yaml')) as ifile:
        hostname_dict = load(ifile, Loader=Loader)
    computers = {}
    for computer, cargs in hostname_dict['platforms'].items():
        kwargs = {k:v for k, v in cargs.items() if k != 'kwargs'}
        for kwarg, key in cargs.get('kwargs',{}).items():
            kwargs[kwarg] = hostname_dict[key]
        kwargs.update(extra_kwargs)
        computers[computer] = {k:v for k, v in kwargs.items()}
    return computers, hostname_dict