"""
Routines for performing calculations, extraction, etc.
"""

def opt_genesis(geom, box_x, box_y, box_z):
    """
    Optimize the system with Genesis.

    Args:
        geom: name of the system
        box_x, box_y, box_z: box size.
    """
    from BigDFT.Interop.GenesisInterop import GenesisCalculator
    from os.path import join
    from glob import glob
    from gentool.calcs import genesis_restraints
    calc = GenesisCalculator(exe="spdyn", skip=True)
    params = {}
    params.update(genesis_restraints())
    tfile = ",".join(glob(f"toppar/{geom}/*.rtf"))
    pfile = ",".join(glob(f"toppar/{geom}/*.prm"))
    sfile = ",".join(glob(f"toppar/{geom}/*.str"))
    params["INPUT"] = {"topfile": tfile,
                       "parfile": pfile,
                       "strfile": sfile,
                       "reffile": f"{geom}.pdb"}
    params["OUTPUT"] = {"rstfile": f"{geom}-opt.rst",
                        "dcdfile": f"{geom}-opt.dcd"}
    params["ENERGY"] = {"forcefield": "CHARMM",
                        "electrostatic": "PME",
                        "switchdist": 10.0,
                        "cutoffdist": 12.0,
                        "pairlistdist": 13.5,
                        "vdw_force_switch": "Yes",
                        "pme_nspline": 4}
    params["MINIMIZE"] = {"method": "SD",
                          "nsteps": 40000,
                          "rstout_period": 40000,
                          "crdout_period": 40000}
    params["CONSTRAINTS"] = {"rigid_bond": "No",
                             "fast_water": "No",
                             "shake_tolerance": 1e-10}
    params["BOUNDARY"] = {"type": "PBC",
                          "box_size_y": box_y, 
                          "box_size_x": box_x, 
                          "box_size_z": box_z}
    log = calc.run(inp=params, name=geom,
                   psf=join("inp", f"{geom}.psf"), 
                   pdb=join("inp", f"{geom}.pdb"))
    return log.info["POTENTIAL_ENE"]


def equil_genesis(geom, box_x, box_y, box_z):
    """
    Equilibrate the system with Genesis.

    Args:
        geom: name of the system
        box_x, box_y, box_z: box size.
    """
    from BigDFT.Interop.GenesisInterop import GenesisCalculator
    from os.path import join
    from glob import glob
    from gentool.calcs import genesis_restraints
    calc = GenesisCalculator(exe="spdyn", skip=True)
    params = {}
    params.update(genesis_restraints())
    tfile = ",".join(glob(f"toppar/{geom}/*.rtf"))
    pfile = ",".join(glob(f"toppar/{geom}/*.prm"))
    sfile = ",".join(glob(f"toppar/{geom}/*.str"))
    params["INPUT"] = {"topfile": tfile,
                       "parfile": pfile,
                       "strfile": sfile,
                       "reffile": f"{geom}.pdb",
                       "rstfile": f"{geom}-opt.rst"}
    params["OUTPUT"] = {"rstfile": f"{geom}-equil.rst",
                        "dcdfile": f"{geom}-equil.dcd"}
    params["ENERGY"] = {"forcefield": "CHARMM",
                        "electrostatic": "PME",
                        "switchdist": 10.0,
                        "cutoffdist": 12.0,
                        "pairlistdist": 13.5,
                        "vdw_force_switch": "Yes",
                        "pme_nspline": 4}
    params["DYNAMICS"] = {"integrator": "VVER",
                          "timestep": 0.001,
                          "nsteps": 250000,
                          "eneout_period": 1000,
                          "rstout_period": 250000,
                          "crdout_period": 50000,
                          "nbupdate_period": 10}
    params["ENSEMBLE"] = {"ensemble": "NVT",
                          "tpcontrol": "BUSSI",
                          "temperature": 303.15}
    params["BOUNDARY"] = {"type": "PBC"}
    params["CONSTRAINTS"] = {"rigid_bond": "Yes",
                             "fast_water": "Yes",
                             "shake_tolerance": 1e-10}
    log = calc.run(inp=params, name=geom + "_equil",
                   psf=f"{geom}.psf", 
                   pdb=f"{geom}.pdb")
    return log.info["POTENTIAL_ENE"]

def prod_common(geom):
    from glob import glob

    params = {}
    tfile = ",".join(glob(f"toppar/{geom}/*.rtf"))
    pfile = ",".join(glob(f"toppar/{geom}/*.prm"))
    sfile = ",".join(glob(f"toppar/{geom}/*.str"))
    params["INPUT"] = {"topfile": tfile,
                       "parfile": pfile,
                       "strfile": sfile,
                       "reffile": f"{geom}.pdb"}
    params["ENERGY"] = {"forcefield": "CHARMM",
                        "electrostatic": "PME",
                        "switchdist": 10.0,
                        "cutoffdist": 12.0,
                        "pairlistdist": 13.5,
                        "vdw_force_switch": "Yes",
                        "pme_nspline": 4}
    params["DYNAMICS"] = {"integrator": "VRES",
                          "timestep": 0.002,
                          "eneout_period": 1000,
                          "crdout_period": 1000000,
                          "nbupdate_period": 10,
                          "elec_long_period": 2,
                          "thermostat_period": 10,
                          "barostat_period": 10}
    params["ENSEMBLE"] = {"ensemble": "NPT",
                          "tpcontrol": "BUSSI",
                          "pressure": 1.0,
                          "temperature": 303.15,
                          "isotropy": "ISO"}
    params["BOUNDARY"] = {"type": "PBC"}
    params["CONSTRAINTS"] = {"rigid_bond": "Yes",
                             "shake_tolerance": 1e-10,
                             "fast_water": "Yes"}
    return params


def prod1_genesis(geom, box_x, box_y, box_z):
    """
    Run one production step of the system with Genesis.

    Args:
        geom: name of the system
        box_x, box_y, box_z: box size.
    """
    from gentools.calcs import prod_common
    from BigDFT.Interop.GenesisInterop import GenesisCalculator
    from os.path import join
    calc = GenesisCalculator(exe="spdyn", skip=True)
    parms = prod_common(geom)
    params["INPUT"]["rstfile"] = f"{geom}-equil.rst"
    params["OUTPUT"] = {"rstfile": f"{geom}-prod1.rst",
                        "dcdfile": f"{geom}-prod1.dcd"}
    params["DYNAMICS"]["nsteps"] = 20000000
    params["DYNAMICS"]["rstout_period"] = 20000000
    log = calc.run(inp=params, name=geom + "_prod1",
                   psf=f"{geom}.psf", 
                   pdb=f"{geom}.pdb")
    return log.info["POTENTIAL_ENE"]

def prod2_genesis(geom, box_x, box_y, box_z):
    """
    Run one production step of the system with Genesis.

    Args:
        geom: name of the system
        box_x, box_y, box_z: box size.
    """
    from gentools.calcs import prod_common
    from BigDFT.Interop.GenesisInterop import GenesisCalculator
    from os.path import join
    calc = GenesisCalculator(exe="spdyn", skip=True)
    parms = prod_common(geom)
    params["INPUT"]["rstfile"] = f"{geom}-prod1.rst"
    params["OUTPUT"] = {"rstfile": f"{geom}-prod2.rst",
                        "dcdfile": f"{geom}-prod2.dcd"}
    params["DYNAMICS"]["nsteps"] = 20000000
    params["DYNAMICS"]["rstout_period"] = 20000000
    log = calc.run(inp=params, name=geom + "_prod2",
                   psf=f"{geom}.psf", 
                   pdb=f"{geom}.pdb")
    return log.info["POTENTIAL_ENE"]

def prod3_genesis(geom, box_x, box_y, box_z):
    """
    Run one production step of the system with Genesis.

    Args:
        geom: name of the system
        box_x, box_y, box_z: box size.
    """
    from gentools.calcs import prod_common
    from BigDFT.Interop.GenesisInterop import GenesisCalculator
    from os.path import join
    calc = GenesisCalculator(exe="spdyn", skip=True)
    parms = prod_common(geom)
    params["INPUT"]["rstfile"] = f"{geom}-prod2.rst"
    params["OUTPUT"] = {"rstfile": f"{geom}-prod3.rst",
                        "dcdfile": f"{geom}-prod3.dcd"}
    params["DYNAMICS"]["nsteps"] = 10000000
    params["DYNAMICS"]["rstout_period"] = 10000000
    log = calc.run(inp=params, name=geom + "_prod3",
                   psf=f"{geom}.psf", 
                   pdb=f"{geom}.pdb")
    return log.info["POTENTIAL_ENE"]

def calc_bigdft(geom, charge, implicit=False, geom_dir='snapshots',outdir='.'):
    """
    Calculate a snapshot with BigDFT.

    Args:
        geom: name of the snapshot
        charge: total charge of the system.
        outdir: output directory of the logfile (typicallythe temporary file system)
        geom_dir: directory where the snapshots are.
        implicit: boolean indicating the implicit solvation usage.
    """
    from BigDFT.Calculators import SystemCalculator
    from BigDFT.Inputfiles import Inputfile
    from BigDFT.IO import read_pdb
    from os.path import join

    with open(join(geom_dir,f"{geom}.pdb")) as ifile:
        sys = read_pdb(ifile, include_chain=True)

    inp = Inputfile()
    inp.set_xc("PBE") 
    inp.set_hgrid(0.45) 
    inp.set_psp_nlcc()
    inp.charge(charge)
    if implicit:
        inp.set_implicit_solvent()
    inp["import"] = "linear"

    calc = SystemCalculator(skip=True)
    log = calc.run(sys=sys, input=inp, name=geom, run_dir="scr", outdir=outdir)

    return log.energy

def remove_solution(sys, dist):
    """
    Remove solution molecules that are not within a given distance.

    Args:
        sys: the system to modify.
        dist: distance cutoff for keeping a water molecule (Bohr).
    """
    from scipy.spatial import KDTree
    from BigDFT.Systems import System

    natoms = len(list(sys.get_atoms()))
    ions = ["SOD", "CLA"]
    water = ["TIP"]
    ion_and_water = ions + water

    # Setup the KDTree
    poslist = []
    frag_lookup = []
    for fragid, frag in sys.items():
        for at in frag:
            poslist.append(at.get_position())
            frag_lookup.append(fragid)
    tree = KDTree(poslist)

    # Extract
    newsys = System()
    wid = 0
    for fragid, frag in sys.items():
        key = fragid.split(":")[0].split("-")[1]
        if key in ions:
            continue
        elif key in water:
            targetpost = [x.get_position() for x in sys[fragid]]
            ndist, nearest = tree.query(targetpost, k=10,
                                        distance_upper_bound=dist)
            nearest = list(set(nearest.flatten()))
            nearest = [frag_lookup[i] for i in nearest if i < natoms]
            nearest = [x for x in nearest 
                       if not any(y in x for y in ion_and_water)]
            if len(nearest) > 0:
                newsys[f"W-WAT:{wid}"] = sys[fragid]
                wid += 1
        elif key in ["017"]:
            newsys["C" + fragid.replace("017", "DAR")] = sys[fragid]
        else:
            newsys[fragid] = sys[fragid]
    return newsys

def extract(geom, out):
    """
    Extract out a snapshot.

    Args:
        geom: this is the prefix of the pdb file of the original system.
        out: this is the prefix used for the dcd file to extract from.
    """
    from BigDFT.IO import write_pdb
    from gentool.sup import read_pdb
    from BigDFT.Atoms import AU_to_A
    from MDAnalysis.coordinates.DCD import DCDReader
    from os import mkdir
    from os.path import join
    from copy import deepcopy
    import warnings
    from Bio import BiopythonWarning
    from gentool.calcs import remove_solution
    import yaml
    from gentool.calcs import atomize
    warnings.filterwarnings("ignore")
    warnings.simplefilter('ignore', BiopythonWarning)

    # Folders
    try:
        mkdir("snapshots")
    except:
        pass

    # Read the system
    with open(join("inp", f"{geom}.pdb")) as ifile:
        sys = read_pdb(ifile, include_chain=True, charmm_format=True)

    # Trajectory
    traj = []
    try:
        reader = DCDReader(out + ".dcd")
    except:
        return 0
    for frame in reader:
        traj.append(deepcopy(sys))
        for at, p in zip(traj[-1].get_atoms(), frame.positions):
            at.set_position(p, units="angstroem")

    # Remove Solution and Write
    with open('inp/drv.yaml') as ifile:
        view = {'C-INH:'+str(i+1): list(v.values())[0]
                for i, v in enumerate(yaml.load(ifile, Loader=yaml.Loader))}
    for i, sys in enumerate(traj):
        psys = remove_solution(sys, 3.0 / AU_to_A)
        atomize(psys,'C-DAR:101',resname='DRV')
        psysview = {k:v for k,v in view.items()}
        psysview.update({f:[f] for f in psys if 'C-' not in f})
        psys = psys.reform_superunits(mapping=psysview)
        psys.to_file(f"snapshots/{out}-{i}.pdb")
    return len(traj)

def atomize(sys, frag, iat=1, resname='LIG'):
    from BigDFT.Fragments import Fragment
    from BigDFT import BioQM
    iat = iat
    ch, name, num = BioQM.construct_frag_tuple(frag)
    for at in sys.pop(frag):
        sys[ch+'-'+resname+':'+str(iat)] = Fragment([at])
        iat += 1
    return iat

def postprocess(jobname, outdir, geom_dir='snapshots', **kwargs):
    from BigDFT.BioQM import construct_frag_tuple, BioSystem
    from BigDFT.IO import read_pdb
    from os.path import join
    from gentool.sup import serialize_system
    import warnings
    warnings.filterwarnings("ignore")
    fname = join(geom_dir, jobname+'.pdb')
    bname = join(geom_dir, jobname+'-fixed.pdb')
    with open(fname) as ifile:
        sys=read_pdb(ifile,include_chain=True)
    for frag in list(sys):
        if 'HSD' in frag:
            key=frag.replace('HSD','HIS')
            sys[key]=sys.pop(frag)
    sys.to_file(bname)
    sys = BioSystem(bname)
    target_chains = None
    fragmentation_groups = []
    if len(sys.sequences_to_fragments) > 0:
        target_chains={construct_frag_tuple(seq[0]).chain:[iseq]
                   for iseq, seq in enumerate(sys.sequences_to_fragments)}
        for frag in sys.unmatched_fragments:
            if 'WAT' in frag:
                continue
            ch,res,num = construct_frag_tuple(frag)
            target_chains[str(num)]=[frag]
        fragmentation_groups = [sys.unmatched_fragments] + sys.sequences_to_fragments
    return serialize_system(jobname, bname, outdir, 
                     tgt_chs=target_chains, fragmentation_groups=fragmentation_groups,
                     **kwargs)

def genesis_restraints():
    """
    This returns some parameters used by genesis to restraint the system during
    optimization / equilibration.
    """
    params = {}
    params["SELECTION"] = {"group1": "(ai:1-3220) and backbone",
                           "group2": 
                           "(ai:1-3220) and not backbone and not hydrogen"}
    params["RESTRAINTS"] = {"nfunctions": 2,
                            "function1": "POSI",
                            "constant1": 1.0,
                            "select_index1": 1,
                            "function2": "POSI",
                            "constant2": 0.1,
                            "select_index2": 2}
    return params
