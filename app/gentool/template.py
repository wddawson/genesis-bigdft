wisteria_spec="""
    submitter: pjsub
    # this is for both odyssey and 
    template: |
        #!/bin/sh
        # #cores_per_node:default=48#  #feature of the machine
        #------ pjsub option --------#
        #PJM -L rscgrp=#queue:default=regular-a#
        #PJM -L node=#nodes#
        #PJM -L gpu=#gpu#
        #PJM --mpi proc=#mpi:optional=False#
        #PJM --omp thread=#omp:optional=False#
        #PJM -L elapse=#time:format=time:optional=False#
        #PJM -g #group:default=jh210022a#
        #PJM -j

        module #module_preload#
        module load #modules#
        export PREFIX=#prefix:default=/work/jh210022a/q25020/build-odyssey/suite#
        export BIGDFT_MPIRUN="mpiexec #mpiexec_extra:default=-machinefile $PJM_O_NODEINF -n $PJM_MPI_PROC -npernode $PJM_PROC_BY_NODE#"
        export FUTILE_PROFILING_DEPTH=0
        export FUTILE_DEBUG_MODE=#futile_debug:default=0#
        #path_modification:default=source $PREFIX/bin/bigdftvars.sh#
        export GENESIS_ROOT=#upstream_prefixes#/bin
        export GENESIS_MPIRUN=$BIGDFT_MPIRUN
       
        #------- Program execution -------#   

    frontend_template: |
      #!/bin/bash
      module #module_preload#
      module load #modules# 2>/dev/null
      export PREFIX=#prefix:default=/work/jh210022a/q25020/build-odyssey/suite#
      export FUTILE_PROFILING_DEPTH=0
      export FUTILE_DEBUG_MODE=#futile_debug:default=0#
      source $PREFIX/bin/bigdftvars.sh
      export GENESIS_ROOT=#upstream_prefixes#/bin
      export GENESIS_MPIRUN=$BIGDFT_MPIRUN
      #command:optional=False#

    odyssey_gnu_nompi:
      fc: gfortran
      fcflags: -fPIC
      cc: gcc
      cflags: -fPIC
      cxx: g++
      modules: fj
      python_interpreter: python
      sourcedir: /home/q25020/bigdft-suite
      tarballdir:        /home/q25020/bigdft-upstream-tarballs
      checkoutroot:      /work/jh210022a/q25020/build-odyssey/checkout
      builddir:          /work/jh210022a/q25020/build-odyssey/builddir
      upstream_prefixes: /work/jh210022a/q25020/build-odyssey/upstream
      prefix:            /work/jh210022a/q25020/build-odyssey/suite

    odyssey:
      fc: mpifrt
      fcflags: -SSL2BLAMP -Kfast,openmp,noautoobjstack -fPIC
      cc: mpifcc
      cflags: -g -std=gnu99 -O2 -fPIC
      cxx: mpiFCC
      linalg: -fjlapackex -SCALAPACK
      configure_line: F77=frt FFLAGS="-Kfast -fPIC" --build=x86_64-redhat-linux --host=sparc-fujitsu-linux LIBS="-SSL2BLAMP -Kfast,openmp -Nlibomp" --without-archives
      module_preload: purge
      modules: fj python
      python_interpreter: python
      sourcedir: /home/q25020/bigdft-suite
      mpiexec_extra: " "
      group: jh210022o
      queue: regular-o
      cmakeargs_update: > 
          {'ntpoly': ' -DFORTRAN_ONLY=Yes -DCMAKE_Fortran_FLAGS_RELEASE="-SSL2BLAMP -Kfast,openmp" -DCMAKE_Fortran_COMPILER="mpifrt" -DCMAKE_C_COMPILER="mpifcc" -DCMAKE_CXX_COMPILER="mpiFCC" -DCMAKE_Fortran_MODDIR_FLAG="-M"'}
      tarballdir:        /work/jh210022o/q25020/bigdft-upstream-tarballs
      checkoutroot:      /work/jh210022o/q25020/build/checkout      
      builddir:          /work/jh210022o/q25020/build/builddir
      upstream_prefixes: /work/jh210022o/q25020/build/upstream
      prefix:            /work/jh210022o/q25020/build/suite

    aquarius:
      fc: mpif90
      fcflags: -O2 -g -fPIC -fopenmp
      cc: mpicc
      cflags: -O2 -g -fPIC
      ompflags: -fopenmp
      cxx: mpicxx
      linalg:  -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl
      configure_line: --enable-cuda-gpu --enable-opencl "NVCC_FLAGS=--compiler-options -fPIC" --enable-single --enable-gpu --with-cuda=$CUDA_PATH LAPACK_LIBS="-L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl" --enable-dynamic-libraries LIBS=-lstdc++ 
      #gpu_line: 
      build_conditions: --conditions=-vdw
      module_preload: purge
      modules: aquarius python/3.9.18 ompi mkl cuda
      python_interpreter: python
      sourcedir: /home/q25020/bigdft-suite
      tarballdir:        /home/q25020/bigdft-upstream-tarballs
      checkoutroot:      /work/jh210022a/q25020/build-aquarius/checkout
      builddir:          /work/jh210022a/q25020/build-aquarius/builddir
      upstream_prefixes: /work/jh210022a/q25020/build-aquarius/upstream
      prefix:            /work/jh210022a/q25020/build-aquarius/suite
"""

computers_database="""
localhost:
   template: |
      #!/bin/bash
      # #cores_per_node:default=8#  #feature of the machine
      export MPI=#mpi:optional=False#
      export OMP_NUM_THREADS=#omp:optional=False#
      export PREFIX=#prefix:default=/opt/bigdft/install#
      export BIGDFT_MPIRUN="#mpirun:default=mpirun -np# $MPI"
      export FUTILE_PROFILING_DEPTH=0
      source $PREFIX/bin/bigdftvars.sh  
      export GENESIS_ROOT=$BIGDFT_ROOT
      export GENESIS_MPIRUN=$BIGDFT_MPIRUN
      export I_MPI_FABRICS=shm #for oneapi container
      export PYTHON=#python_interpreter:default=python3#
      #extra_lines#

irene: ### Irene TGCC
    submitter: ccc_msub
    template: |
      #!/bin/bash
      # #cores_per_node:default=128#  #feature of the machine
      #MSUB -n #mpi:optional=False#
      #MSUB -N #nodes:default={(mpi*omp)/cores_per_node}#
      #MSUB -c #omp:optional=False#
      #MSUB -t #time:optional=False#
      #MSUB -A #project:default=gen12049#
      #MSUB -r #jobname:default=job#
      #MSUB -q #queue:default=rome#
      #MSUB -m #filesystem:default=work,scratch#
      #MSUB -o #output:default={jobname}.o#
      #MSUB -e #error:default={jobname}.e#
      module #module_preload#
      module load #modules:default=python3 cmake inteloneapi/24.0.0 mpi/intelmpi/24.0.0 mkl/24.0.0#

      set -x
      cd $BRIDGE_MSUB_PWD

      export PREFIX=#prefix:default=/ccc/work/cont003/gen12049/genovesl/binaries/intel_oneapi_mpi/suite#
      export MKL_DEBUG_CPU_TYPE=5
      export BIGDFT_MPIRUN=ccc_mprun
      export FUTILE_PROFILING_DEPTH=0
      export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
      source $PREFIX/bin/bigdftvars.sh  
      export GENESIS_ROOT=$BIGDFT_ROOT
      export GENESIS_MPIRUN=$BIGDFT_MPIRUN

    frontend_template: |
      #!/bin/bash
      module #module_preload#
      module load #modules:default=python3 cmake inteloneapi/24.0.0 mpi/intelmpi/24.0.0 mkl/24.0.0#
      export PREFIX=#prefix:default=/ccc/work/cont003/gen12049/genovesl/binaries/intel_oneapi_mpi/suite#
      export MKL_DEBUG_CPU_TYPE=5
      export FUTILE_PROFILING_DEPTH=0
      source $PREFIX/bin/bigdftvars.sh  
      export GENESIS_ROOT=$BIGDFT_ROOT
      export GENESIS_MPIRUN=$BIGDFT_MPIRUN
      #command:optional=False#

    gnu_mpi:
      fc: mpif90
      fcflags: -m64 -I${MKLROOT}/include -O2 -fPIC -fopenmp
      cc: mpicc
      cflags: -O2 -g -fPIC
      ompflags: -fopenmp
      cxx: mpicxx
      linalg: -m64  -L${MKLROOT}/lib -Wl,--no-as-needed -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl
      configure_line: --enable-dynamic-libraries LIBS="-lstdc++"
      module_preload: purge
      modules: python3 cmake gnu mpi mkl/24.0.0
      python_interpreter: python
      sourcedir: /ccc/work/cont003/drf/genovesl/1.9.5
      tarballdir:        /ccc/work/cont003/gen12049/genovesl/bigdft-upstream-tarballs
      builddir:          /ccc/work/cont003/gen12049/genovesl/binaries/compile/gnu_mpi
      upstream_prefixes: /ccc/work/cont003/gen12049/genovesl/binaries/gnu_mpi/upstream
      prefix:            /ccc/work/cont003/gen12049/genovesl/binaries/gnu_mpi/suite

    intel_mpi:
      fc: mpif90 -fc=ifort
      fcflags: -I${MKLROOT}/include -O2 -fPIC -qopenmp
      cc: mpicc -cc=icc
      cflags: -O2 -g -fPIC
      ompflags: -qopenmp
      cxx: mpicxx -cc=icpc
      linalg:  -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
      configure_line: --enable-dynamic-libraries F77=ifort
      build_conditions: --conditions=-vdw
      module_preload: purge
      modules: python3 cmake intel mpi mkl
      python_interpreter: python
      sourcedir: /ccc/work/cont003/drf/genovesl/1.9.5
      tarballdir:        /ccc/work/cont003/gen12049/genovesl/bigdft-upstream-tarballs
      builddir:          /ccc/work/cont003/gen12049/genovesl/binaries/compile/intel_mpi
      upstream_prefixes: /ccc/work/cont003/gen12049/genovesl/binaries/intel_mpi/upstream
      prefix:            /ccc/work/cont003/gen12049/genovesl/binaries/intel_mpi/suite

    intel_intelmpi:
      fc: mpif90 -fc=ifort
      fcflags: -I${MKLROOT}/include -O2 -fPIC -qopenmp
      cc: mpicc -cc=icc
      cflags: -O2 -g -fPIC
      ompflags: -qopenmp
      cxx: mpicxx -cc=icpc
      linalg:  -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
      configure_line: --enable-dynamic-libraries F77=ifort
      build_conditions: --conditions=-vdw
      module_preload: purge
      modules: python3 cmake intel mpi/intelmpi mkl
      python_interpreter: python
      sourcedir: /ccc/work/cont003/drf/genovesl/1.9.5
      tarballdir:        /ccc/work/cont003/gen12049/genovesl/bigdft-upstream-tarballs
      builddir:          /ccc/work/cont003/gen12049/genovesl/binaries/compile/intel_intelmpi
      upstream_prefixes: /ccc/work/cont003/gen12049/genovesl/binaries/intel_intelmpi/upstream
      prefix:            /ccc/work/cont003/gen12049/genovesl/binaries/intel_intelmpi/suite

    intel_oneapi:
      fc: ifx
      fcflags: -I${MKLROOT}/include -O2 -fPIC -qopenmp
      cc: icx
      cflags: -O2 -g -fPIC
      ompflags: -qopenmp
      cxx: icpx
      linalg: -L${MKLROOT}/lib -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
      configure_line: --enable-dynamic-libraries F77=ifx
      module_preload: purge
      modules: python3 cmake inteloneapi/24.0.0 mpi/intelmpi/24.0.0 mkl/24.0.0
      python_interpreter: python
      sourcedir: /ccc/work/cont003/drf/genovesl/1.9.5
      tarballdir:        /ccc/work/cont003/gen12049/genovesl/bigdft-upstream-tarballs
      builddir:          /ccc/work/cont003/gen12049/genovesl/binaries/compile/intel_oneapi
      upstream_prefixes: /ccc/work/cont003/gen12049/genovesl/binaries/intel_oneapi/upstream
      prefix:            /ccc/work/cont003/gen12049/genovesl/binaries/intel_oneapi/suite

    intel_oneapi_mpi:
      fc: mpif90 -fc=ifx
      fcflags: -I${MKLROOT}/include -O2 -fPIC -qopenmp
      cc: mpicc -cc=icx
      cflags: -O2 -g -fPIC
      ompflags: -qopenmp
      cxx: icpx
      linalg: -L${MKLROOT}/lib -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
      configure_line: --enable-dynamic-libraries F77=ifx
      module_preload: purge
      modules: python3 cmake inteloneapi/24.0.0 mpi/intelmpi/24.0.0 mkl/24.0.0
      python_interpreter: python
      sourcedir: /ccc/work/cont003/drf/genovesl/1.9.5
      tarballdir:        /ccc/work/cont003/gen12049/genovesl/bigdft-upstream-tarballs
      builddir:          /ccc/work/cont003/gen12049/genovesl/binaries/compile/intel_oneapi_mpi
      upstream_prefixes: /ccc/work/cont003/gen12049/genovesl/binaries/intel_oneapi/upstream
      prefix:            /ccc/work/cont003/gen12049/genovesl/binaries/intel_oneapi_mpi/suite
"""
