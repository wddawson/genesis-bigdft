def read_pdb(ifile, include_chain=False, disable_warnings=False,
             ignore_connectivity=False, ignore_unit_cell=False,
             charmm_format=False):
    """
    Read a system from a PDB file.

    Args:
      ifile (TextIOBase): the file to read from.
      disable_warnings (bool): whether to print warnings about possible file
          issues.
      include_chain (bool): include the chain id if True
      ignore_connectivity (bool): if True do not store the information about
          the connectivity matrix.
      ignore_unit_cell (bool): assumes free boundary conditions if true.
      charmm_format (bool): Accepts Charmm-format.

    Warning:
       This will read in the connectivity information from the pdb as well.
       However, a pdb file does not provide any information about the bond
       order. Thus, the bond order of each bond will be set to one.

    Returns:
      (BigDFT.Systems.System): the system file.
    """
    from BigDFT.Fragments import Fragment
    from BigDFT.Systems import System
    from warnings import warn
    from BigDFT.UnitCells import UnitCell

    # First pass read in the atoms.
    sys = System()
    lookup = {}
    sys.conmat = {}
    found = False

    for line in ifile:
        try:  # See if there is an atom on this line
            if line[:4] == "ATOM" or line[:6] == "HETATM":
                at, atid, fragid = _process_atom(line,
                                                 include_chain=include_chain,
                                                 charmm_format=charmm_format)

                # We can ignore lone pairs
                if at.sym == "Lp":
                    continue

                # Add to the system
                if fragid not in sys:
                    sys[fragid] = Fragment()
                sys[fragid] += [at]

                # Build the lookup table
                lookup[atid] = (fragid, len(sys[fragid]) - 1)

            elif line[:6] == "CONECT" and not ignore_connectivity:
                found = True
                split = _split_line(line, len(lookup))
                (fragid, atnum) = lookup[int(split[1])]

                for at2 in split[2:]:
                    fragid2, atnum2 = lookup[int(at2)]

                    if fragid not in sys.conmat:
                        sys.conmat[fragid] = []
                        for i in range(0, len(sys[fragid])):
                            sys.conmat[fragid].append({})

                    sys.conmat[fragid][atnum][(fragid2, atnum2)] = 1.0

            elif line[:6] == "CRYST1" and not ignore_unit_cell:
                a = float(line[7:15])
                b = float(line[16:24])
                c = float(line[25:33])
                alpha = float(line[34:40])
                beta = float(line[41:47])
                gamma = float(line[48:54])

                sys.cell = UnitCell([a, b, c], units="angstroem")

                if not disable_warnings:
                    if (alpha != 90 or beta != 90 or gamma != 90):
                        warn("Cell angles must be 90 degrees", UserWarning)

        except IndexError:  # For shorter lines
            continue

    if ignore_unit_cell:
        sys.cell = UnitCell()

    if not found:
        sys.conmat = None
    else:
        # for any connectivity not specified we give default values.
        for fragid in sys:
            if fragid not in sys.conmat:
                sys.conmat[fragid] = []
                for i in range(0, len(sys[fragid])):
                    sys.conmat[fragid].append({})

    if not disable_warnings:
        if sum([len(x) for x in sys.values()]) == 0:
            warn("Warning: zero atoms found", UserWarning)

    return sys

def _split_line(line, limit):
    split = line.split()
    # print line, split
    if len(split) < 3:  # there should be at least two atoms
        tmp = line.lstrip('CONECT')
        split = ['CONECT'] + _split_malformed_connection(tmp)
    resplit = [split[0]]
    for at2 in split[1:]:
        atom = int(at2)
        if atom > limit:
            atoms = _split_malformed_connection(at2)
        else:
            atoms = [atom]
        resplit += atoms
    return resplit


def _process_atom(line, include_chain=False, charmm_format=False):
    """
    This processes a line of a pdb file and extracts information about an atom.

    Returns:
      (BigDFT.Atoms.Atom, int, str): return the Atom on this line, its id in
      the pdb, and the fragment it belongs to. It may include the chain id.
    """
    from BigDFT.Atoms import Atom
    # Get the basic information about this atom
    sym = line[76:78].strip().capitalize()

    if "1-" in sym:
        sym = sym.replace("1-", "")
    if "1+" in sym:
        sym = sym.replace("1+", "")

    fullname = line[12:16]

    name = fullname.strip()

    xpos = float(line[30:38])
    ypos = float(line[38:46])
    zpos = float(line[46:54])

    at = Atom({"sym": sym, "r": [xpos, ypos, zpos], "name": name,
               "units": "angstroem"})

    # Get the atom id for building the lookup table
    atid = int(line[6:11])

    # Information about the residue
    resname = line[17:20]
    chain = line[20:22].strip()
    if charmm_format and 'PRO' == line[72:75]:
        chain = line[75]

    resid = str(int(line[22:27]))
    fragid = resname+":"+resid
    if include_chain:
        fragid = chain+'-'+fragid

    return at, atid, fragid


def serialize_system(jobname, filename, outdir, reserialize=False,
                     reinvestigate=False, tgt_chs=None, options={},
                     fragmentation_groups=[], initial_view=None,
                     nthreads=64, atomized_version=True, **useless_kwargs):
    from BigDFT import BioQM
    from os.path import join, isfile
    from futile.Utils import tarfile_is_coherent

    def section(sys, chs):
        sec = []
        nchains = len(sys.sequences_to_fragments)
        for ch in chs:
            if isinstance(ch, int):
                if ch < nchains:
                    sec += [f for f in sys.sequences_to_fragments[ch]
                            if f is not None]
                else:
                    sec += [sys.unmatched_fragments[ch-nchains]]
            else:
                sec.append(ch)
        return sec

    def verify_tarfile(filename):
        try:
            ok = tarfile_is_coherent(filename)
        except Exception as e:
            print('Error for "'+ filename+'": '+str(e))
            ok = False

        return ok

    logfile = join(outdir, 'log-'+jobname+'.yaml')
    archive = jobname + '-1.2.tar.bz2'
    true_options = dict(ignore_connectivity=True,
                        ignore_unit_cell=True)
    true_options.update(options)
    sys = None
    if not verify_tarfile(archive) or reinvestigate:
        if not verify_tarfile(jobname+'-1.1.tar.bz2') or reserialize:
            BioQM.serialize_biosystem(jobname+'-1.1.tar.bz2', filename,
                                      logfile, version='1.1',
                                      **true_options)
        sys = BioQM.load(jobname+'-1.1.tar.bz2',
                         options=true_options)
        cutoffs = [0.1, 0.08, 0.075, 0.07, 0.06, 0.05, 0.045, 0.04, 0.035, 0.03]
        sys.chessboards(cutoffs, initial_view=initial_view,
                        initial_groups=fragmentation_groups)
        if tgt_chs is not None:
            for name, chs in tgt_chs.items():
                tgt = section(sys, chs)
                env = None
                sys.system_target_interaction_investigation(tgt,
                                                            nthreads=nthreads,
                                                            cutoff_bo=0.001,
                                                            cutoff_el=10,
                                                            name=name,
                                                            environment=env)
                dist = sys.fragment_distances(tgt)
                env = [f for f in sys if f not in tgt]
                dist += sys.fragment_distances(env)
                if name == '':
                    distname = 'substrate_distance'
                else:                    
                    distname = name+'_substrate_distance'
                sys.set_fragment_quantities(distname, dist)

        BioQM.BioSystemSerialization(sys, version='1.2').dump(archive)

def bsys_to_database(bsys):
    from numpy import nan
    logdata=extract_log_info(bsys.logfile)
    energies=['energy','trH','hartree_energy','trVxc','XC_energy',
              'ionic_energy']
    interactions = ['interactions','bond_orders', 'hartree_interactions',
                    'ionic_interactions', 'long_range_interactions']
    attrs = {e: getattr(bsys.logfile, e, nan) for e in energies}
    logdata.update(attrs)
    logdata.update({int_type+'_check':doublesum(getattr(bsys,int_type))
                    for int_type in interactions})
    logdata.update({str(ich)+'_'+int_type+'_check':doublesum_chain(getattr(bsys,int_type),
                                                                  bsys.sequences_to_fragments[ich])
                    for int_type in interactions
                    for ich in range(len(bsys.sequences_to_fragments))})
    allints = {}
    for it in bsys._fragment_quantities:
        v = bsys.fragment_values(it)
        if len(v) == len(bsys):
            allints[it] = sum(v)
    logdata.update(allints)
    return logdata
