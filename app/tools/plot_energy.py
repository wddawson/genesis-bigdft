from sys import argv
from matplotlib import pyplot as plt
from remotemanager import Dataset
from yaml import load, SafeLoader
import seaborn as sns

if __name__ == "__main__":
	key = argv[1]
	with open("tools/energy_plots.yaml") as ifile:
		data = load(ifile, Loader=SafeLoader)
	energies = {}
	for k, v in data[key].items():
		energies[k] = []
		for name in v:
			temp = Dataset.from_file(f"datasets/{name}")
			temp.fetch_results()
			energies[k].extend(temp.results[0])
	fig, axs = plt.subplots(1, 1, figsize=(5, 3.5))
	sns.set()
	for k, v in energies.items():
		xvals = [x * data["time_steps"][key] for x in range(len(v))]
		axs.plot(xvals, v, '-', label=k)
	axs.set_ylabel("Energy", fontsize=12)
	axs.set_xlabel(data["xlabel"][key], fontsize=12)
	axs.legend()
	fig.tight_layout()
	axs.set_title(data["title"][key])
	plt.savefig(f"{key}-energy.png", bbox_inches="tight")