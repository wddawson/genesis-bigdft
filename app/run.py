from sys import argv
from gentool.common import get_computers, get_remote_base
from gentool import calcs
from remotemanager import Dataset, SanzuFunction, Computer
from futile.Utils import ensure_dir
from BigDFT.Interop import RemoteManagerInterop as RM
from time import sleep

def check_finished():
    return all(dsg.is_finished) and \
           all(dsb.is_finished) and \
           len(dss.is_finished) == len(dsb.is_finished)


if __name__ == "__main__":
    # Get the arguments
    rid = argv[1]
    box_size = int(argv[2])
    charge = int(argv[3])
    task = argv[4]
    local_dir = f"datasets/stream-{rid}"
    remote_dir = get_remote_base(local_dir=f"{rid}")
    scratch_dir = get_remote_base(local_dir=f"{rid}",permanent=False, machine='o')
    ensure_dir("datasets")
    dsargs = {"remote_dir": remote_dir, "local_dir": local_dir,
              "verbose": False}

    # Build the computers
    computers, wdict = RM.get_host_specs('wisteria', uri='gentool')
    cpu = Computer(**computers['cpu'])
    gpu = Computer(**computers['gpu'])
    front = Computer(**computers['front'])
    front.command = ':'

    # Genesis Dataset
    func = getattr(calcs, f"{task}_genesis")
    dsg = Dataset(func, url=gpu, 
    			  name=f"{task}-{rid}",
    			  dbfile=f"datasets/{task}-{rid}-genesis.yaml",
             	  extra_files_send=["gentool", "inp", f"toppar"],
             	  **dsargs, **wdict['aquarius'])
    dsg.append_run({"geom": rid, 
    				"box_x": box_size, "box_y": box_size, "box_z": box_size},
                    #mpi=8, nodes=1, omp=9, queue="regular-a", time=60 * 60 * 47)
    				 mpi=8, nodes=1, omp=9, queue="debug-a", time=60 * 60 * 0.5)
    				# mpi=1, gpu=1, omp=9, queue="share", time=60 * 60 * 1)

    # Extraction
    extract = SanzuFunction(url=front, 
    	                    name=f"extract-{rid}",
    						dbfile=f"datasets/{task}-{rid}-extract.yaml",
    						extra_files_send=["gentool"],
                            extra_files_recv="snapshots", 
                            **dsargs, **wdict['aquarius'])(calcs.extract)

    # BigDFT
    dsb = Dataset(calcs.calc_bigdft, url=cpu,
    			  name=f"linear-{rid}", 
    			  extra_files_send=["gentool"],
    			  dbfile=f"datasets/{task}-{rid}-bigdft.yaml",
    			  **dsargs, **wdict['odyssey'])

    # Serialize
    dss = Dataset(calcs.postprocess, url=cpu, 
                  name=f"serialize-{rid}",
                  extra_files_send=["gentool"],
                  dbfile=f"datasets/{task}-{rid}-serialize.yaml",
                  **dsargs, **wdict['odyssey'])

    # Do the loop
    dsg.run()
    while True:
        finished = check_finished()
        nsnaps = extract(rid, f"{rid}-{task}")
        try:
            nsnaps = int(nsnaps)
        except:
            continue
        extract.dataset.wipe_runs()
        for i in range(nsnaps):
            dsb.append_run({"geom": f"{rid}-{task}-{i}", 
                            "charge": charge,
            	            "implicit": False, 'outdir': scratch_dir},
                            nodes=64, omp=12, mpi=128,
                            time=60 * 60 * 4, cores_per_node=48)
                            # nodes=1, omp=1, mpi=48,
                            # time=60 * 60 * 1, cores_per_node=48)

        dsb.run()
        for i, run in enumerate(dsb.runners):
            if run.is_finished:
                dss.append_run({"jobname": f"{rid}-{task}-{i}", 
                                "outdir": scratch_dir,
                                "nthreads": 48},
                                nodes=1, omp=1, mpi=48,
                                time=60 * 60 * 2, cores_per_node=48)
        dss.run()
        sleep(60)
        if finished:
            break
