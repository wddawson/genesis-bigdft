"""
Spring computers.
"""
from remotemanager import BaseComputer

cpu_template = """#!/bin/bash
#PBS -S /bin/bash
#PBS -q #QUEUE:default=winter2#
#PBS -l nodes=1:ncpus=#CPU#

export I_MPI_FABRICS=shm:ofa # For Infiniband
export I_MPI_FALLBACK=0
export I_MPI_HYDRA_BOOTSTRAP=rsh
export I_MPI_PIN_DOMAIN=omp

# Form MPI command
export nprocs=#MPI#
export nhosts=1
export nprocs_per_host=$nprocs
export mpirun="mpiexec.hydra -machinefile $PBS_NODEFILE -n $nprocs -perhost $nprocs_per_host"
export OMP_NUM_THREADS=#OMP#

# Modules
. /opt/intel/oneapi/setvars.sh
conda activate #conda#

cd $PBS_O_WORKDIR

# BigDFT Info
export PYTHONPATH=$PYTHONPATH:~/binaries/bdft/install/lib/python3.11/site-packages/
export BIGDFT_ROOT=~/binaries/bdft/install/bin/
export BIGDFT_MPIRUN=$mpirun
export NTCHEM_ROOT=~/binaries/nekir/bin
export NTCHEM_MPIRUN=$BIGDFT_MPIRUN
"""

gpu_template = """#!/bin/bash
#SBATCH --partition=#PARTITION#
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=#MPI#
#SBATCH --cpus-per-task=#OMP#
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# Modules
. /opt/intel/oneapi/setvars.sh
conda activate #conda#

# BigDFT Info
export PYTHONPATH=$PYTHONPATH:~/binaries/bdft/install/lib/python3.11/site-packages/
export GENESIS_ROOT="/home/dawson/Genesis/bin"
"""

front_template = """#!/bin/bash
. /opt/intel/oneapi/setvars.sh
conda activate #conda#
export PYTHONPATH=$PYTHONPATH:~/binaries/bdft/install/lib/python3.11/site-packages/
"""

cpu = BaseComputer(template=cpu_template, submitter="qsub",
                   host="spring.r-ccs27.riken.jp")
gpu = BaseComputer(template=gpu_template, submitter="sbatch",
                   host="spring.r-ccs27.riken.jp")
front = BaseComputer(template=front_template,
                     host="spring.r-ccs27.riken.jp")