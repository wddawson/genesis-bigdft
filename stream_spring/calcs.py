"""
Routines for performing calculations, extraction, etc.
"""
def calc_genesis(geom):
    from BigDFT.Interop.GenesisInterop import GenesisCalculator
    from os.path import join
    from glob import glob
    calc = GenesisCalculator(exe="spdyn", skip=True)
    params = {}
    tfile = ",".join(glob("toppar/*.rtf"))
    pfile = ",".join(glob("toppar/*.prm"))
    sfile = ",".join(glob("toppar/*.str"))
    params["INPUT"] = {"topfile": tfile,
                       "parfile": pfile,
                       "strfile": sfile}
    params["OUTPUT"] = {"rstfile": "1uao.rst", "dcdfile": "1uao.dcd"}
    params["ENERGY"] = {"forcefield": "CHARMM",
                        "electrostatic": "PME",
                        "switchdist": 10.0,
                        "cutoffdist": 12.0,
                        "pairlistdist": 12.5,
                        "pme_nspline": 4}
    params["MINIMIZE"] = {"method": "SD",
                          "nsteps": 20000,
                          "rstout_period": 20000,
                          "crdout_period": 2000}
    params["BOUNDARY"] = {"type": "PBC",
                          "box_size_y": 37, 
                          "box_size_x": 37, 
                          "box_size_z": 37}
    log = calc.run(inp=params, name=geom,
                   psf=join("inp", f"{geom}.psf"), 
                   pdb=join("inp", f"{geom}.pdb"))
    return log.info["POTENTIAL_ENE"]

def calc_bigdft(geom, charge):
    from BigDFT.Calculators import SystemCalculator
    from BigDFT.Inputfiles import Inputfile
    from BigDFT.IO import read_pdb

    with open(f"snapshots/{geom}.pdb") as ifile:
        sys = read_pdb(ifile)

    inp = Inputfile()
    inp.set_xc("PBE") ; inp.set_hgrid(0.45) ; inp.set_psp_nlcc()
    inp.charge(charge)
    inp["import"] = "linear"

    calc = SystemCalculator()
    log = calc.run(sys=sys, input=inp, name=geom, run_dir="scr")

    return log.energy

def calc_ntchem(geom, basis, charge):
    from BigDFT.Interop.NTChem.Inputfiles import Inputfile
    from BigDFT.Interop.NTChem.Calculators import SystemCalculator
    from BigDFT.IO import read_pdb

    with open(f"snapshots/{geom}.pdb") as ifile:
        sys = read_pdb(ifile)
    
    calc = SystemCalculator()

    inp = Inputfile()
    inp.set_basic_rhf()
    inp.set_linear_scaling()
    inp["scf"].icharg = charge

    log = calc.run(sys, inp, basis, name=geom)
    return log.energy

def extract(geom):
    from BigDFT.IO import write_pdb, read_pdb
    from BigDFT.Systems import System
    from MDAnalysis.coordinates.DCD import DCDReader
    from os import mkdir
    from os.path import join
    from copy import deepcopy
    import warnings
    warnings.filterwarnings("ignore")

    # Folders
    try:
        mkdir("snapshots")
    except:
        pass

    # Read the system
    with open(join("inp", f"{geom}.pdb")) as ifile:
        sys = read_pdb(ifile)

    # Trajectory
    traj = []
    try:
        reader = DCDReader(geom + ".dcd")
    except:
        return 0
    for frame in reader:
        traj.append(deepcopy(sys))
        for at, p in zip(traj[-1].get_atoms(), frame.positions):
            at.set_position(p, units="angstroem")

    # Write
    for i, sys in enumerate(traj):
        psys = System()
        for fragid, frag in sys.items():
            fname, fid = fragid.split("-")[-1].split(":")
            if fname not in ["SOD", "CLA", "TIP"]:
                psys[fragid] = frag
        with open(f"snapshots/{geom}-{i}.pdb", "w") as ofile:
            write_pdb(psys, ofile)

    return len(traj)
