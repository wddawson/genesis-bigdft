# Genesis - BigDFT

The purpose of this repository to to contain examples and results related
to the coupling of the [Genesis](https://www.r-ccs.riken.jp/labs/cbrt/) and 
[BigDFT](https://l_sim.gitlab.io/bigdft-doc/index.html) simulation codes.
The coupling will be obtained through a combination of the PyBigDFT
and remotemanager Python packages [1]. 

## Coupling Concept

The concept for combining these codes is shown in the below figure. Genesis
will be used to drive classical molecular dynamics simulations. During this
run, remotemanager will inspect the trajectory file produced by Genesis.
When new configurations are available, it will automatically generate a 
BigDFT calculation of that pose. Finally the data will be combined for analysis.

![concept](concept.png)

## Preparing Input with CHARMM-GUI

First, we will introduce a simple tutorial of using 
[charmm-gui](https://charmm-gui.org/) [2] to prepare the needed inputs for Genesis.
The outputs of this process are in the `a194_data` folder. After making an
account, click on the left panel `input generator`. Then select
`solution builder`.

![step 1: solution builder](images/charmm1.png)

We will use the HIV protease bound to a drug molecule for this example. Type
in the PDB code (`a194`) and hit next. On the next page, you need to select
the relevant species. There are two copies of the system, so we should only
select one. Make sure to include the ligand, and you can also include
the crystallographic waters as needed. 

![step 2: include species](images/charmm2.png)

Continue with the default settings. One thing to modify is to switch the
ion type to NaCl from KCl. This is done by selecting NaCl, clicking 
"add simple ion type", removing the KCl, and then hitting the 
"calculate solvent composition" button.

![step 3: switch ion type](images/charmm3.png)

Continue to the next page (this will take some time to process). Now
we're at the `Solvator` step. Here, you should note the size of the box we're
going to use. Genesis will require this input information, as it isn't contained
in the `PDB` file for some reason. Though we could retrive this information
using other means.

![step 4: note the box size](images/charmm4.png)

Continue to the next page (this also takes a long time). We've now reached
the step where we can select the forcefield we want to use and setup
a simulation. The default is the charmm forcefield. It will prepare inputs
that first run minimization, then equilibration, followed by a production
dynamics runs. Click the checkbox next to `GENESIS` to get the input files
we will want. After clicking next, the charmm-gui step is done, so we can
download the tar file with the results.

Let's finish by talking about the files we need. Untar the file. There
are two relevant folders inside: `toppar` and `genesis`. `toppar` includes
all of the forcefield parameters for this system. You will need to put
this next to your notebook as an extra file to send. From the `genesis`
folder, we just need `step3_input.pdb` and `step3_input.psf`. However,
it may also be useful to read the `.inp` files to learn the basic parameters of
minimization, equilibration, and production simulations. In these input
files, you can also find the cell parameters mentioned earlier, in case you
lost them.

## Project Data

These are the basics of the project. Individual notebooks and folders can
then focus on the tasks we have ahead of ourself:

1) `stream_spring`: an example of the streaming workflow.
2) A workflow notebook for a big enough computer should be created.
3) We need to create profiles for the Genesis interop module that mimic
the suggested parameters of CHARMM-GUI.

## References

[1] Dawson, William, Louis Beal, Laura E. Ratcliff, Martina Stella, Takahito Nakajima, and Luigi Genovese. "Exploratory data science on supercomputers for quantum mechanical calculations." Electronic Structure 6, no. 2 (2024): 027003.

[2] S. Jo, T. Kim, V.G. Iyer, and W. Im (2008) CHARMM-GUI: A Web-based Graphical User Interface for CHARMM. J. Comput. Chem. 29:1859-1865 